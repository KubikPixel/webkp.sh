# webkp.sh

```txt
##############################################
#               _     _               _      #
# __      _____| |__ | | ___ __   ___| |__   #
# \ \ /\ / / _ \ '_ \| |/ / '_ \ / __| '_ \  #
#  \ V  V /  __/ |_) |   <| |_) |\__ \ | | | #
#   \_/\_/ \___|_.__/|_|\_\ .__(_)___/_| |_| #
#                         |_|                #
##############################################
# A simple web scrap archive works on macOS  #
##############################################
```

A simple but configurable web scraper backup & archiving shell script that runs on *Linux* & *macOS*.

## Why this script

A customer of mine wanted a way without admin access on his macOS or [homebrew](https://brew.sh/) an easy way to *archive* (*backup*) his website. It is not a real backup but only a scrap of a snapshots.

Sure [wget](https://www.gnu.org/software/wget/) could be installed more elegant and system wide but the circumstances did not allow that for reasons. Therefore this *local installation*, as of this script also.

This script is especially suitable for archiving static sites but for this would be so or so again better Git suitable, which I was not allowed to install by the way at the customer.

## Install on macOS

Type on the Terminal the follow lines to install the script.

```sh
$ mkdir -p ~/.local/lib
$ xcode-select --install
$ git clone https://codeberg.org/KubikPixel/webkp.sh.git ~/.local/lib/webkp
$ chmod u+x ~/.local/lib/webkp/webkp.sh
$ ~/.local/lib/webkp/webkp.sh -i
```

## Use this script

* For help type `webkp -h`
* For install wget type `webkp -i`
* For config webkp.sh type `webkp -c`

See source for more details `¯\_(ツ)_/¯`

GPG signature for this script you can find [here](webkp.sh.gpg) and my public PGP key and fingerprint is on my [website](https://thunix.net/~kubikpixel/).

## License

This project is under the MIT licens for more details see the [LICENSE](LICENSE) file.

```txt
Copyright (c) 2021 KubikPixel

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
