#!/usr/bin/env bash

                   ##############################################
                   #               _     _               _      #
                   # __      _____| |__ | | ___ __   ___| |__   #
                   # \ \ /\ / / _ \ '_ \| |/ / '_ \ / __| '_ \  #
                   #  \ V  V /  __/ |_) |   <| |_) |\__ \ | | | #
                   #   \_/\_/ \___|_.__/|_|\_\ .__(_)___/_| |_| #
                   #                         |_|                #
                   ##############################################
                   # A simple web scrap archive works on macOS  #
                   ##############################################
#
# =============================================================================
# Program: webkp.sh
# Description: A simple but configurable web scraper backup & archiving shell
# script that runs on Linux & macOS.
# Version: v0.1.2
# Language: bash
# License: MIT
# Git: https://codeberg.org/KubikPixel/webkp.git
# Signature: https://codeberg.org/KubikPixel/webkp/raw/branch/main/webkp.sh.sig
# Author: KubikPixel <kubikpixel@dismail.de>
# Website: https://thunix.net/~kubikpixel/
# =============================================================================
#
WEBKPVER="v0.1.2"
WGETVER="1.21.1"
WGETDIR=$HOME/.local/lib/wget/bin

webkp_install() {
  local curdir=$(pwd)
  mkdir -p $WGETDIR
  curl -O https://ftp.gnu.org/gnu/wget/wget-$WGETVER.tar.gz
  tar -zxvf wget-$WGETVER.tar.gz
  cd wget-$WGETVER/
  ./configure --with-ssl=openssl
  make
  make install
  rm -rf ~/Downloads/wget*
  cd $curdir
  return $?
}

webkp_config() {
  if [ ! -f $WEBKPDIR/webkp.conf ]; then
    local website="https://url.example.tld"
    local level=5
    local bkpdir="~/Downloads/webkp"
    local archive="user@archive.server.tld:port"
  else
    source $WEBKPDIR/webkp.conf
    local website=$WEBKP_WEBSITE
    local level=$WEBKP_LEVEL
    local bkpdir=$WEBKP_BKPDIR
    local archive=$WEBKP_ARCHIVE
  fi

  read -p "Website to download ($website): " webs
  read -p "Website deep level ($level): " levl
  read -p "Directory for archive ($bkpdir): " bkpd
  read -p "Directory for archive ($archive): " arch
  if [ -z $webs ]; then
    echo "WEBKP_WEBSITE=$website" > $WEBKPDIR/webkp.conf
  else
    echo "WEBKP_WEBSITE=$webs" > $WEBKPDIR/webkp.conf
  fi
  if [ -z $levl ]; then
    echo "WEBKP_LEVEL=$level" >> $WEBKPDIR/webkp.conf
  else
    echo "WEBKP_LEVEL=$levl" >> $WEBKPDIR/webkp.conf
  fi
  if [ -z $bkpd ]; then
    echo "WEBKP_BKPDIR=$bkpdir" >> $WEBKPDIR/webkp.conf
  else
    echo "WEBKP_BKPDIR=$bkpd" >> $WEBKPDIR/webkp.conf
  fi
  if [ -z $arch ]; then
    echo "WEBKP_ARCHIVE=$archive" >> $WEBKPDIR/webkp.conf
  else
    echo "WEBKP_ARCHIVE=$arch" >> $WEBKPDIR/webkp.conf
  fi
  return $?
}

webkp_check() {
  if ! type curl; then
    printf -- '%s\n' "[ERROR] webkp: This command requires 'curl', please install it."
    return 1
  fi
  if ! type openssl; then
    printf -- '%s\n' "[ERROR] webkp: This command requires 'openssl', please install it."
    return 1
  fi
  if ! type make; then
    printf -- '%s\n' "[ERROR] webkp: This command requires 'make', please install it."
    return 1
  fi
  if ! type wget; then
    printf -- '%s\n' "[WARNING] webkp: This command requires 'wget', please install it."
    return 1
  fi
  if ! type tar; then
    printf -- '%s\n' "[ERROR] webkp: This command requires 'tar', please install it."
    return 1
  fi
  if ! type gzip; then
    printf -- '%s\n' "[ERROR] webkp: This command requires 'gzip', please install it."
    return 1
  fi
  if ! type rsync; then
    printf -- '%s\n' "[ERROR] webkp: This command requires 'rsync', please install it."
    return 1
  fi
  if [ ! -f $WEBKPDIR/webkp.conf ]; then
    printf -- '%s\n' "[WARNING] webkp: The config not exist, please config the application."
  fi
  return $?
}

webkp_help() {
    printf -- '%s\n' "webkp.sh: A simple web scrap archive script works on Linux & macOS" "Version: $WEBKPVER" "-h    this Help" "-i    Install the tool(s)" "-u    Updates the tool(s)" "-c    Config the tool" "-t    Test the components" "-v    Version information"
  return $?
}

webkp_init() {
  if [ ! -d $WEBKPDIR ]; then
    printf -- '%s\n' "[WARNING] webkp: The application is not installed, please install now."
    return 1
  elif [ -f $WEBKPDIR/webkp.conf ]; then
    source $WEBKPDIR/webkp.conf
    export PATH=$PATH:$WEBKPDIR
    alias webkp="$WEBKPDIR/webkp.sh"
    mkdir -p $WEBKP_ARCHIVE
  else
    printf -- '%s\n' "[WARNING] webkp: The config not exist, please config the application."
    return 1
  fi
  return $?
}

webkg_run() {
  local curdir=$(pwd)
  local bkpver=$(date '+%Y%m%d-%H%M')
  local bkpfile="webkp_$bkpver"
  mkdir -p $WEBKP_BKPDIR/webkp_$bkpver
  cd $WEBKP_BKPDIR/$bkpfile
  wget -l $WEBKP_LEVEL -o $bkpfile.log -rv $WEBKP_WEBSITE
  cd ..
  tar -czvf $bkpfile.tar.gz $bkpfile/*
  rm -rf $bkpfile
  rsync -b $WEBKP_BKPDIR/$bkpfile.tar.gz $WEBKP_ARCHIVE
  cd $curdir
  return $?
}

# main
webkp_init
case $1 in
  -h)
    webkp_help
    ;;
  -i)
    webkp_install
    ;;
  -u)
    printf -- '%s\n' "[INFO] webkp: Updates not implementet yet, try to install again."
    ;;
  -c)
    webkp_config
    ;;
  -t)
    webkp_check
    ;;
  -v)
    printf -- '%s\n' "[INFO] webkp.sh - A simple web scrap archive script works on Linux & macOS" "Version: $WEBKPVER" "Author: KubikPixel <kubikpixel@dismail.de>"
    ;;
  *)
    webkg_run
    ;;
esac
